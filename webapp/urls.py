"""webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from keystone.views import node_list_view, edge_list_view, node_detail_view, home_view, visualization_view, data_view

urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^visualization/$', visualization_view, name='visualization'),
    url(r'^admin/', admin.site.urls),
    url(r'^node/$', node_list_view, name='node-list'),
    url(r'^node/(?P<pk>[-\w]+)/$', node_detail_view, name='node-detail'),
    url(r'^edge/', edge_list_view, name='edge-list'),
    url(r'^data/', data_view, name='data'),
]
