# -*- coding: utf-8 -*-
from collections import defaultdict, Counter
from itertools import product, combinations, chain
from math import sin, cos, radians

import numpy

from .models import Edge, Node

__author__ = 'mateuszb'

r = 5
angle_delta = 5


def compute_coordinates(angle):
    return r * cos(radians(angle)), r * sin(radians(angle))


def collect_pairs(list_of_targets):
    pair_counter = Counter()
    for l in list_of_targets:
        unique_tokens = sorted(set(l))
        combos = combinations(unique_tokens, 2)
        pair_counter += Counter(combos)
    return pair_counter


def create_connection_matrix(from_type, to_type):
    edges = Edge.objects.filter(source_node__type=from_type,
                                target_node__type=to_type) \
        .values_list('source_node__name', 'target_node__name')
    data_dict = defaultdict(list)
    for source_node, target_node in edges:
        data_dict[source_node].append(target_node)
    list_of_targets = []
    for k, v in data_dict.items():
        list_of_targets.append(v)
    total_list = chain(*list_of_targets)
    return Counter(total_list), collect_pairs(list_of_targets)


def calculate_points(n, radius_factor=1):
    golden_angle = numpy.pi * (3 - numpy.sqrt(5))
    theta = golden_angle * numpy.arange(n)
    z = numpy.linspace(1 - 1.0 / n, 1.0 / n - 1, n)
    radius = numpy.sqrt(1 - z * z) * radius_factor
    points = numpy.zeros((n, 3))
    points[:, 0] = radius * numpy.cos(theta)
    points[:, 1] = radius * numpy.sin(theta)
    points[:, 2] = z * radius_factor
    return points


def create_matrix(edges):
    source_nodes = list(set(edges.values_list('source_node', flat=True)))
    matrix_dict = dict()
    for node in source_nodes:
        matrix_dict[node] = defaultdict(lambda: 0)
        node_targets = edges.filter(source_node=node).values_list('target_node', flat=True)
        for target in node_targets:
            matrix_dict[node][target] += 1
    return matrix_dict



