import json

from django.core.management.base import BaseCommand
from django.db.models import F
from django.db.utils import IntegrityError

from keystone.models import Edge, Node

from bs4 import BeautifulSoup


class Command(BaseCommand):
    help = 'Dump data from database to json'

    def add_arguments(self, parser):
        parser.add_argument('--filename')
        parser.add_argument('--delete')

    def handle(self, *args, **options):
        nodes = list(
            Node.objects.all().filter(type__in=["person", "expertise"]).values(
                "id", "type", "name"))
        edges = list(
            Edge.objects.filter(source_node__type="person",
                                target_node__type='expertise').annotate(
                source=F('source_node'),
                target=F(
                    'target_node')).values(
                "source", "target"))
        data_dict = {
            "nodes": nodes,
            "links": edges
        }
        with open("keystone.json", "w+") as fp:
            json.dump(data_dict, fp)
        self.stdout.write("Success!")
