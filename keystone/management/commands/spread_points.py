# -*- coding: utf-8 -*-
import json

import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

__author__ = 'mateuszb'

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from keystone.models import Edge, Node

from bs4 import BeautifulSoup


def calculate_points(n, radius_factor=1):
    golden_angle = numpy.pi * (3 - numpy.sqrt(5))
    theta = golden_angle * numpy.arange(n)
    z = numpy.linspace(1 - 1.0 / n, 1.0 / n - 1, n)
    radius = numpy.sqrt(1 - z * z) * radius_factor
    points = numpy.zeros((n, 3))
    points[:, 0] = radius * numpy.cos(theta)
    points[:, 1] = radius * numpy.sin(theta)
    points[:, 2] = z * radius_factor
    return points


class Command(BaseCommand):
    help = 'spread points on the sphere'

    def handle(self, *args, **options):
        global_node_dict = dict()
        node_list = []
        edge_list = []
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        nodes = Node.objects.filter(type='person')
        n = nodes.count()
        points = calculate_points(n)
        for idx, node in enumerate(nodes):
            pts = points[idx]
            global_node_dict[node.id] = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            node_list.append({
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            })
        # ax.scatter(points[:, 0], points[:, 1], points[:, 2], c="0.5")
        # print(points)
        nodes = Node.objects.filter(type='expertise')
        n = nodes.count()
        points = calculate_points(n, radius_factor=2)
        for idx, node in enumerate(nodes):
            pts = points[idx]
            global_node_dict[node.id] = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            node_list.append({
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            })
        with open('nodes.json', 'w+') as fp:
            json.dump(node_list, fp)
        edges = Edge.objects.filter(source_node__type='person',
                                    target_node__type='expertise')
        for edge in edges:
            source_node = global_node_dict[edge.source_node.id]
            target_node = global_node_dict[edge.target_node.id]
            edge_list.append({
                "source": {
                    "x": source_node["x"],
                    "y": source_node["y"],
                    "z": source_node["z"],
                },
                "target": {
                    "x": target_node["x"],
                    "y": target_node["y"],
                    "z": target_node["z"],
                }
            })
        with open('edges.json', 'w+') as fp:
            json.dump(edge_list, fp)
        # ax.scatter(points[:, 0], points[:, 1], points[:, 2], c="1")
        # plt.show()
