from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from django.views.generic import ListView, DetailView, TemplateView

from keystone.utils import calculate_points
from .forms import EdgeFilterForm, NodeFilterForm
from .models import Edge, Node


class HomeView(TemplateView):
    template_name = 'home.html'


home_view = HomeView.as_view()


class VisualizationView(TemplateView):
    template_name = 'keystone/visualization.html'

    def get_context_data(self, **kwargs):
        context = super(VisualizationView, self).get_context_data(**kwargs)
        types = list(set(Node.objects.values_list('type', flat=True)))
        filter_dict = dict()
        for t in types:
            if t != 'person':
                filter_dict[t] = list(Node.objects.filter(type=t).values_list(
                    'name', flat=True))
        context.update({"form": NodeFilterForm()})
        context.update({"filters": filter_dict})
        return context


visualization_view = VisualizationView.as_view()


class EdgeListView(ListView):
    model = Edge
    paginate_by = 10

    def get_queryset(self):
        name = self.request.GET.get('name')
        source_type = self.request.GET.get('source_node_type')
        target_type = self.request.GET.get('target_node_type')
        if name:
            qs = Edge.objects.filter(Q(source_node__name__icontains=name) |
                                     Q(target_node__name__icontains=name))
        else:
            qs = Edge.objects.all()
        if source_type:
            qs = qs.filter(source_node__type=source_type)
        if target_type:
            qs = qs.filter(target_node__type=target_type)
        return qs

    def get_context_data(self, **kwargs):
        context = super(EdgeListView, self).get_context_data(**kwargs)
        context.update({"form": EdgeFilterForm()})
        return context


edge_list_view = EdgeListView.as_view()


class NodeListView(ListView):
    model = Node
    paginate_by = 10

    def get_queryset(self):
        name = self.request.GET.get('name')
        node_type = self.request.GET.get('node_type')
        if name:
            qs = Node.objects.filter(name__icontains=name)
        else:
            qs = Node.objects.all()
        if node_type:
            qs = qs.filter(type=node_type)
        return qs

    def get_context_data(self, **kwargs):
        context = super(NodeListView, self).get_context_data(**kwargs)
        context.update({"form": NodeFilterForm()})
        return context


node_list_view = NodeListView.as_view()


class NodeDetailView(DetailView):
    model = Node


node_detail_view = NodeDetailView.as_view()


class DataView(View):
    def get(self, request, *args, **kwargs):
        global_node_dict = dict()
        node_list = []
        edge_list = []

        choice = request.GET.get('node_type', 'expertise')

        nodes = Node.objects.filter(type='person')  # wsrod wszystkich wezlow, wybierz wszystkie te o type='person'
        # ogranicz węzły tylko do tych, które występują w relacjach person-expertise
        nodes = [node for node in nodes if
                 Edge.objects.filter(source_node=node,
                                     target_node__type=choice).exists()]
        # liczba punktów to liczba węzłów, które spełniają powyższe warunki
        n = len(nodes)
        # oblicz współrzędne punktów przy rozłożeniu w sposób równomierny,
        # radius_factor to zmienna skalująca sfery - sfera dla węzlow typu person powinna byc wieksza niz dla expertise
        points = calculate_points(n, radius_factor=2)
        # dla kazdego wezla stowrz slownik z odpowiednimi informacjami oraz jego pozycja i dodaj do ogolnej listy
        for idx, node in enumerate(nodes):
            pts = points[idx]
            node_dict = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "img_src": node.img_src,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            global_node_dict[node.id] = node_dict
            node_list.append(node_dict)
        # to samo dla innej ilosci wezlow
        nodes = Node.objects.filter(type=choice)
        n = nodes.count()
        # tutaj juz nie skalujemy - mniejsza sfera
        points = calculate_points(n)
        for idx, node in enumerate(nodes):
            pts = points[idx]
            node_dict = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            global_node_dict[node.id] = node_dict
            node_list.append(node_dict)
        # znajdz wszystkie relacje person-expertise
        edges = Edge.objects.filter(source_node__type='person',
                                    target_node__type=choice)
        for edge in edges:
            # w global_node_dict przechowujemy wspolrzedne wybrane dla danego wezla
            source_node = global_node_dict[edge.source_node.id]
            target_node = global_node_dict[edge.target_node.id]
            edge_list.append({
                "source": {
                    "x": source_node["x"],
                    "y": source_node["y"],
                    "z": source_node["z"],
                },
                "target": {
                    "x": target_node["x"],
                    "y": target_node["y"],
                    "z": target_node["z"],
                }
            })
        return JsonResponse([node_list, edge_list], safe=False)


data_view = DataView.as_view()
